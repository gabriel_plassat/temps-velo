var el = document.createElement("div");
el.innerHTML =
  cities.reduce(
    (memo, next) =>
      `${memo}<li><a href="${next}/"> ${
        next[0].toUpperCase() + next.slice(1)
      }</a></li>`,
    "<ul>"
  ) + "</ul>";

document.getElementById("index").appendChild(el);
